# ThumbnailApp

A website template viewer with a sliding window to navigate over the thumbnails/images. This application is responsive and is developed using ReactJS and NodeJS 

Clone the repo and follow below steps

Install packages

1. Run ```npm install``` from root of project
2. Run ```npm install``` from client

Start the app by executing ```npm run dev``` from root of the project. This will start up both React and Node


For executing test cases, run ```npm test``` from client
