const express = require('express');
const app = express();
const cors = require('cors');

var corsOption = {
    origin: true,
    methods: 'GET, HEAD, PUT, PATCH, POST, DELETE'
};

app.use(cors(corsOption));

app.get('/', (req, res) => res.send('Thumbnail API running'));
app.use('/api/templates', require('./routes/api/template'));

const PORT = process.env.PORT || 5000;

app.listen(PORT);