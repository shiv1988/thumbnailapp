import reducer from './reducer';

import { 
    SELECT_IMAGE_SUCCESS,
    NEXT_SUCCESS,
    PREVIOUS_SUCCESS
 } from './actions';

 describe('reducer', () => {
     it('should return the initial state', () => {
         expect(reducer(undefined, {})).toEqual({
            templates: null,
            selectedImage: null,
            templateOfFour: null,
            firstIndex: 0,
            lastIndex: 0,
            next: 'next disabled',
            previous: 'previous disabled'
         });
     })
 })

 describe('Click on Next', () => {
     it('should enable Previous button', () => {
         expect(reducer({
            templateOfFour: null,
            firstIndex: 0,
            lastIndex: 0,
            next: 'next disabled',
            previous: 'previous disabled'
         }, {
             type: NEXT_SUCCESS,
             templateOfFour: [{}],
             firstIndex: 4,
             lastIndex: 8,
             next: 'next',
             previous: 'previous'
         })).toEqual({
            templateOfFour: [{}],
            firstIndex: 4,
            lastIndex: 8,
            next: 'next',
            previous: 'previous'
         })
     })
 })

 describe('Click on Previous', () => {
    it('should enable Next button', () => {
        expect(reducer({
           templateOfFour: null,
           firstIndex: 4,
           lastIndex: 8,
           next: 'next disabled',
           previous: 'previous'
        }, {
            type: PREVIOUS_SUCCESS,
            templateOfFour: [{}],
            firstIndex: 0,
            lastIndex: 4,
            next: 'next',
            previous: 'previous disabled'
        })).toEqual({
           templateOfFour: [{}],
           firstIndex: 0,
           lastIndex: 4,
           next: 'next',
           previous: 'previous disabled'
        })
    })
})

describe('Select a thumbnail', () => {
    const selectedElementFirst = {
        title: "Business Site Template - 7111",
        cost: "45.00",
        id: "7111",
        description: "Lorem ipsum dolor sit amet, dictum et quisque aliquet malesuada at, rutrum ac nullam, elit massa facilisis",
        thumbnail: "7111-m.jpg",
        image: "7111-b.jpg"
    }
    const selectedElementSecond = {
        title: "Business Site Template - 7112",
        cost: "55.00",
        id: "7112",
        description: "Laoreet eu amet soluta error a nulla, sed maecenas est risus augue turpis varius, torquent fermentum diam in augue.",
        thumbnail: "7112-m.jpg",
        image: "7112-b.jpg"
    }
    let templates = [selectedElementFirst, selectedElementSecond];
    it('should show the large image of the selected thumbnail', () => {
        expect(reducer({
           selectedImage: selectedElementFirst,
           templates: templates
        }, {
            type: SELECT_IMAGE_SUCCESS,
            selectedImage: selectedElementSecond,
            templates: templates
        })).toEqual({
           selectedImage: selectedElementSecond,
           templates: templates
        })
    })
})