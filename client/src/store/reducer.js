import {
    FETCH_META_SUCCESS, 
    SELECT_IMAGE_SUCCESS,
    NEXT_SUCCESS,
    PREVIOUS_SUCCESS
} from '../store/actions';

/*  
    templateOfFour - stores the meta of 4 or fewer images that are shown in the sliding window
    firstIndex - stores the position of first image in the template of four images
    lastIndex - stores the position of last image in the template of four images
    next & previous - stores the css classes for next and previous button
*/
const initialState = {
    templates: null,
    selectedImage: null,
    templateOfFour: null,
    firstIndex: 0,
    lastIndex: 0,
    next: 'next disabled',
    previous: 'previous disabled'
};

export default function templateReducer(
    state = initialState,
    action
) {
    switch (action.type) {
        case FETCH_META_SUCCESS: 
            return {
                ...state,
                templates: action.payload.meta.templates,
                selectedImage: action.payload.meta.selectedImage,
                templateOfFour: action.payload.meta.templateOfFour,
                lastIndex: action.payload.meta.lastIndex,
                next: action.payload.meta.next
            };
        case SELECT_IMAGE_SUCCESS: 
            return {
                ...state,
                selectedImage: action.selectedImage,
                templates: action.templates
            };
        case NEXT_SUCCESS: 
            return {
                ...state,
                templateOfFour: action.templateOfFour,
                firstIndex: action.firstIndex,
                lastIndex: action.lastIndex,
                next: action.next,
                previous: action.previous
            };
        case PREVIOUS_SUCCESS: 
            return {
                ...state,
                templateOfFour: action.templateOfFour,
                firstIndex: action.firstIndex,
                lastIndex: action.lastIndex,
                next: action.next,
                previous: action.previous
            };
        default: 
            return state;
    }
}