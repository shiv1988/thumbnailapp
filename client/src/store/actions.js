import axios from 'axios';
import { API_ROOT } from '../api-config';

export const FETCH_META_SUCCESS = "FETCH_META_SUCCESS";
export const SELECT_IMAGE_SUCCESS = "SELECT_IMAGE_SUCCESS";
export const NEXT_SUCCESS = "NEXT_SUCCESS";
export const PREVIOUS_SUCCESS = "PREVIOUS_SUCCESS";

export const fetchTemplate = () => {
    return dispatch => {
        axios.get(`${API_ROOT}/templates`)
        .then(res => {
            let meta = {};
            meta.templates = res.data;
            if(meta.templates && meta.templates.length > 0){
                if(meta.templates.length > 4) {
                    meta.lastIndex = 4; 
                    meta.next = 'next';
                }    
                else{
                    meta.lastIndex = meta.templates.length;
                    meta.next = 'next disabled'
                }
                meta.templateOfFour = meta.templates.slice(0, meta.lastIndex);
                meta.templateOfFour.map((ft) => ft.isThumbnailSelected = false);
                meta.templateOfFour[0].isThumbnailSelected = true;
                meta.selectedImage = meta.templates[0];
            }    
            dispatch(fetchTemplateSuccess(meta));
        });
    }
}

export const selectImage = (id, templates) => {
    return dispatch => {
        let selectedImage = {};
        selectedImage = templates.find((t) => t.id === id);
        for(let template of templates) {
          if(template.id === selectedImage.id)
            template.isThumbnailSelected = true;
          else
            template.isThumbnailSelected = false;
        }
        dispatch(selectImageSuccess(selectedImage, templates));
    }
}

export const next = (meta) => {
    return dispatch => {
        if(meta.lastIndex === meta.templates.length)
            return;
        meta.firstIndex = meta.lastIndex;
        if(meta.lastIndex + 4 < meta.templates.length)
            meta.lastIndex += 4;
        else {
            meta.lastIndex = meta.templates.length;
            meta.next = 'next disabled';
        }
        meta.previous = 'previous';
        meta.templateOfFour = meta.templates.slice(meta.firstIndex, meta.lastIndex);
        dispatch(nextSuccess(meta.templateOfFour, meta.firstIndex, meta.lastIndex, meta.next, meta.previous));
    }
}

export const previous = (meta) => {
    return dispatch => {
        if(meta.firstIndex < 4)
            return;
        meta.lastIndex = meta.firstIndex;
        if(meta.firstIndex - 4 < 4 ){
            meta.firstIndex = 0;
            meta.previous = 'previous disabled';
        }
        else
            meta.firstIndex -= 4;
        meta.next = 'next';
        meta.templateOfFour = meta.templates.slice(meta.firstIndex, meta.lastIndex);
        dispatch(previousSuccess(meta.templateOfFour, meta.firstIndex, meta.lastIndex, meta.next, meta.previous));
    }
}

export const fetchTemplateSuccess = meta => ({
    type: FETCH_META_SUCCESS,
    payload: { meta }
});

export const selectImageSuccess = (selectedImage, templates) => ({
    type: SELECT_IMAGE_SUCCESS,
    selectedImage: selectedImage, 
    templates: templates
});

export const nextSuccess = ( templateOfFour, firstIndex, lastIndex, next, previous ) => ({
    type: NEXT_SUCCESS,
    templateOfFour: templateOfFour, 
    firstIndex: firstIndex, 
    lastIndex: lastIndex, 
    next: next, 
    previous: previous
});

export const previousSuccess = ( templateOfFour, firstIndex, lastIndex, next, previous ) => ({
    type: PREVIOUS_SUCCESS,
    templateOfFour: templateOfFour, 
    firstIndex: firstIndex, 
    lastIndex: lastIndex, 
    next: next, 
    previous: previous
});