import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Large from './large';
import Meta from './meta';

configure({adapter: new Adapter()});

describe('<Large />', () => {
    let wrapper;
    const image = '7111-b.jpg';

    beforeEach(() => {
        wrapper = shallow(<Large image={image}/>)
    })

    it('should render one <img /> element', () => {
        expect(wrapper.contains(<img src={require('../../src/images/large/'+image)} alt="Large Template" width="430" height="360" />)).toEqual(true);
    })

    it('should render one <Meta /> element', () => {
        expect(wrapper.find(Meta)).toHaveLength(1);
    })
})