import React from 'react';
import Meta from './meta';

const large = props => {
    return (
        <div className="group">
          <img src={require('../../src/images/large/'+props.image)} alt="Large Template" width="430" height="360" />
          <Meta 
            title = {props.title}
            description = {props.description}
            cost = {props.cost}
            id = {props.id}
            thumbnail = {props.thumbnail}
            image = {props.image}></Meta>
        </div>
    )
}

export default large;