import React from 'react';

const meta = props => {
    return (
        <div className="details">
            <p><strong>Title</strong> {props.title}</p>
            <p><strong>Description</strong> {props.description}</p>
            <p><strong>Cost</strong> {props.cost}</p>
            <p><strong>ID #</strong> {props.id}</p>
            <p><strong>Thumbnail File</strong> {props.thumbnail}</p>
            <p><strong>Large Image File</strong> {props.image}</p>
        </div>
    )
}

export default meta;