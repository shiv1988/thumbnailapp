import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Meta from './meta';

configure({adapter: new Adapter()});

describe('<Meta />', () => {
    let wrapper;
    const { title, description, cost, id, thumbnail, image } = ["Title", "Template", "50.00", "7111", "7111-m.jpg", "7111-b.jpg"];

    beforeEach(() => {
        wrapper = shallow(<Meta />)
    })

    it('should render a Title', () => {
        expect(wrapper.contains(<p><strong>Title</strong> {title}</p>)).toEqual(true);
    })

    it('should render a Description', () => {
        expect(wrapper.contains(<p><strong>Description</strong> {description}</p>)).toEqual(true);
    })

    it('should render a Cost', () => {
        expect(wrapper.contains(<p><strong>Cost</strong> {cost}</p>)).toEqual(true);
    })

    it('should render an Id', () => {
        expect(wrapper.contains(<p><strong>ID #</strong> {id}</p>)).toEqual(true);
    })

    it('should render a Thumbnail file', () => {
        expect(wrapper.contains(<p><strong>Thumbnail File</strong> {thumbnail}</p>)).toEqual(true);
    })

    it('should render a Large image file', () => {
        expect(wrapper.contains(<p><strong>Large Image File</strong> {image}</p>)).toEqual(true);
    })
})