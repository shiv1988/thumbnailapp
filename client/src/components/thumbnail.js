import React from 'react';

const thumbnail = (props) => {
    return (
        <div id="thumbnailContainer">
            <div className={props.isThumbnailSelected ? "thumbnailElement active": "thumbnailElement"} 
                title={props.title} onClick={props.selectImg}>
                <img src={require("../../src/images/thumbnails/"+props.thumbnailImg)} 
                    alt={props.id + "-m"} width="145" height="121" />
                <span>{props.id}</span>
            </div>
        </div>
    )
}

export default thumbnail;