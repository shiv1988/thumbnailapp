import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Thumbnail from './thumbnail';

configure({adapter: new Adapter()});

describe('<Thumbnail />', () => {
    let wrapper;
    let props = {
        title: "Business Site Template - 7111",
        id: "7111",
        thumbnailImg: "7111-m.jpg",
        selectImg: "7111-b.jpg",
        isThumbnailSelected: true
    }

    beforeEach(() => {
        wrapper = shallow(<Thumbnail {...props}/>)
    })

    it('should have an Active class if the thumbnail is selected', () => {
        expect(wrapper.find('.thumbnailElement').hasClass('active')).toEqual(true);
    })

    it('should not have an Active class if the thumbnail is not selected', () => {
        props.isThumbnailSelected = false;
        wrapper = shallow(<Thumbnail {...props}/>)
        expect(wrapper.find('.thumbnailElement').hasClass('active')).toEqual(false);
    })

    it('should render one <img /> element', () => {
         expect(wrapper.contains(<img src={require("../../src/images/thumbnails/"+props.thumbnailImg)} 
         alt={props.id + "-m"} width="145" height="121" />)).toEqual(true);
    })
})