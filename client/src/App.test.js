import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Large from '../src/components/large';
import Thumbnail from '../src/components/thumbnail';
import { App } from './App';

configure({adapter: new Adapter()});

describe('<App />', () => {
    let wrapper;
    const templates = [{
      title: "Business Site Template - 7111",
      cost: "45.00",
      id: "7111",
      description: "Lorem ipsum dolor sit amet, dictum et quisque aliquet malesuada at, rutrum ac nullam, elit massa facilisis",
      thumbnail: "7111-m.jpg",
      image: "7111-b.jpg"
    }]
    let props = {
      title: "Business Site Template - 7111",
      id: "7111",
      thumbnailImg: "7111-m.jpg",
      selectedImage: "7111-b.jpg",
      isThumbnailSelected: true,
      templateOfFour: templates,
      templates: templates
    }

    beforeEach(() => {
        wrapper = shallow(<App dispatch={() => {}} {...props} />)
    })

    it('should render one <Large /> element', () => {
      expect(wrapper.find(Large)).toHaveLength(1);
    })

    it('should render one <Thumbnail /> element if there is only one element in templates of 4', () => {
      expect(wrapper.find(Thumbnail)).toHaveLength(1);
    })

    it('should render two <Thumbnail /> element if there are 2 elements in templates of 4', () => {
      let templates = [{
        title: "Business Site Template - 7111",
        cost: "45.00",
        id: "7111",
        description: "Lorem ipsum dolor sit amet, dictum et quisque aliquet malesuada at, rutrum ac nullam, elit massa facilisis",
        thumbnail: "7111-m.jpg",
        image: "7111-b.jpg"
      },
      {
        title: "Business Site Template - 7111",
        cost: "45.00",
        id: "7111",
        description: "Lorem ipsum dolor sit amet, dictum et quisque aliquet malesuada at, rutrum ac nullam, elit massa facilisis",
        thumbnail: "7111-m.jpg",
        image: "7111-b.jpg"
      }]
      let props = {
        title: "Business Site Template - 7111",
        id: "7111",
        thumbnailImg: "7111-m.jpg",
        selectedImage: "7111-b.jpg",
        isThumbnailSelected: true,
        templateOfFour: templates,
        templates: templates
      }
      wrapper = shallow(<App dispatch={() => {}} {...props} />)
      expect(wrapper.find(Thumbnail)).toHaveLength(2);
    })

    it('should enable Next buttom if there are more than 4 elements', () => {
      let props = {
        title: "Business Site Template - 7111",
        id: "7111",
        thumbnailImg: "7111-m.jpg",
        selectedImage: "7111-b.jpg",
        isThumbnailSelected: true,
        templateOfFour: templates,
        templates: templates,
        firstIndex: 0,
        lastIndex: 4,
        next: 'next',
        previous: 'previous disabled'
      }
      wrapper = shallow(<App dispatch={() => {}} {...props} />)
      expect(wrapper.find('.next').hasClass('disabled')).toEqual(false);
    })
})