import React, { Component } from 'react';
import { connect } from 'react-redux';
import './App.css';
import Thumbnail from '../src/components/thumbnail';
import Large from '../src/components/large';
import { fetchTemplate, selectImage, next, previous } from '../src/store/actions';

export class App extends Component {
  
  componentDidMount() {
    this.props.dispatch(fetchTemplate());
  }

  selectImage = (id) => {
    this.props.dispatch(selectImage(id, this.props.templates));
  }

  next = () => {
    let meta = {
      templates: this.props.templates,
      templateOfFour: this.props.templateOfFour,
      lastIndex: this.props.lastIndex,
      firstIndex: this.props.firstIndex,
      previous: this.props.previous,
      next: this.props.next,
    }
    this.props.dispatch(next(meta));
  }

  previous = () => {
    let meta = {
      templates: this.props.templates,
      templateOfFour: this.props.templateOfFour,
      lastIndex: this.props.lastIndex,
      firstIndex: this.props.firstIndex,
      previous: this.props.previous,
      next: this.props.next,
    }
    this.props.dispatch(previous(meta));
  }

  render(){
    return (
      <div id="container">
      <header>Code Development Project</header>
      {this.props.templates && this.props.selectedImage ?
        <div id="main" role="main">
          <div id="large">
            <Large
              id = {this.props.selectedImage.id}
              title = {this.props.selectedImage.title}
              description = {this.props.selectedImage.description}
              cost = {this.props.selectedImage.cost}
              thumbnail = {this.props.selectedImage.thumbnail}
              image = {this.props.selectedImage.image}></Large>
          </div>
          <div className="thumbnails">
            <div className="group">
              {this.props.templateOfFour.map((template) => {
                return (<Thumbnail
                  key = {template.id}
                  id = {template.id}
                  thumbnailImg = {template.thumbnail}
                  isThumbnailSelected = {template.isThumbnailSelected}
                  selectImg={() => this.selectImage(template.id)}>
                  </Thumbnail>)       
              })}
              <span className={this.props.previous} title="Previous" onClick={this.previous}>Previous</span>
              <span className={this.props.next} title="Next" onClick={this.next}>Next</span>
            </div>
          </div>
        </div> : <div id="TemplateContainer"><span>Image not available</span></div>}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    templates: state.templates,
    selectedImage: state.selectedImage,
    templateOfFour: state.templateOfFour,
    firstIndex: state.firstIndex,
    lastIndex: state.lastIndex,
    next: state.next,
    previous: state.previous
  }
}

export default connect(mapStateToProps)(App);