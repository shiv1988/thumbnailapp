const express = require('express');
const router = express.Router();
const template = require('../../data/templates.json');

router.get('/',(req, res) => res.json(template));

module.exports = router;